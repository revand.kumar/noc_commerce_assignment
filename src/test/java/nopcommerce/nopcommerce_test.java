package nopcommerce;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import jxl.read.biff.BiffException;

public class nopcommerce_test {

	WebDriver driver = null;

	@BeforeClass
	public void launchApplications() {
		driver = new ChromeDriver();
		driver = new ChromeDriver();
		driver.get("https://admin-demo.nopcommerce.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	}
	@Test
	public void Login() {
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		String text = driver.findElement(By.partialLinkText("John Smith")).getText();
		Assert.assertEquals(text, "John Smith");
	}
	@Test(priority = 1)
	public void Categories() throws InterruptedException, BiffException, IOException {
		driver.findElement(By.partialLinkText("Catalog")).click();
		driver.findElement(By.partialLinkText("Categories")).click();
		driver.findElement(By.partialLinkText("Add new")).click();
		File f = new File("home/revand/eclipse/project_nopcommerce/target/file/commerse.xlsx");
		FileInputStream files = new FileInputStream(f);
		XSSFWorkbook workbook = new XSSFWorkbook(files);
		XSSFSheet Sheet1 = workbook.getSheetAt(0);
		int row = Sheet1.getPhysicalNumberOfRows();
		for (int i = 0; i < row; i++) {
			String name = Sheet1.getRow(i).getCell(0).getStringCellValue();
			String des = Sheet1.getRow(i).getCell(1).getStringCellValue();
			driver.findElement(By.id("Name")).sendKeys(name);
			driver.switchTo().frame(0);
			driver.findElement(By.id("tinymce")).sendKeys(des);
			driver.switchTo().defaultContent();
			WebElement element = driver.findElement(By.id("ParentCategoryId"));
			Select s = new Select(element);
			s.selectByIndex(5);
			driver.findElement(By.name("save")).click();
			driver.findElement(By.id("SearchCategoryName")).sendKeys(name);
			driver.findElement(By.id("search-categories")).click();
		}
		String text = driver.findElement(By.xpath("//td[text()='Computers >> Desktops >> Build your own computer']"))
				.getText();
		Assert.assertEquals(text, "Computers >> Desktops >> Build your own computer");
	}
	@Test(priority = 2)
	public void Manufacturers() throws InterruptedException, IOException {
		driver.findElement(By.partialLinkText("Manufacturers")).click();
		driver.findElement(By.partialLinkText("Add new")).click();
		File f = new File("/home/revand/eclipse/project_nopcommerce/target/file/commerse.xlsx");
		FileInputStream files = new FileInputStream(f);
		XSSFWorkbook workbook = new XSSFWorkbook(files);
		XSSFSheet Sheet1 = workbook.getSheetAt(0);
		int row = Sheet1.getPhysicalNumberOfRows();
		for (int i = 0; i < row; i++) {
			String name = Sheet1.getRow(i).getCell(0).getStringCellValue();
			String des = Sheet1.getRow(i).getCell(1).getStringCellValue();
			driver.findElement(By.id("Name")).sendKeys(name);
			driver.switchTo().frame(0);
			driver.findElement(By.id("tinymce")).sendKeys(des);
			driver.switchTo().defaultContent();
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,500)");
			driver.findElement(By.xpath("//input[@title=\"0.0000 USD\"]")).sendKeys("50");
			driver.findElement(By.xpath("//input[@title=\"10000.0000 USD\"]")).sendKeys("70");
			driver.findElement(By.xpath("//input[@title=\"0 \"]")).sendKeys("1");
			driver.findElement(By.name("save")).click();
			driver.findElement(By.name("SearchManufacturerName")).sendKeys("Nokia");
			driver.findElement(By.id("search-manufacturers")).click();
		}
		String text = driver.findElement(By.partialLinkText("Nokia")).getText();
		Assert.assertNotSame(text, "Nokia");
	}
	@Test(priority = 3)
	public void product() throws InterruptedException, IOException {
		driver.findElement(By.partialLinkText("Products")).click();
		driver.findElement(By.id("SearchProductName")).sendKeys("Build your own computer");
		WebElement element1 = driver.findElement(By.id("SearchCategoryId"));
		Select s2 = new Select(element1);
		s2.selectByIndex(39);
		driver.findElement(By.name("search-products")).click();
		Thread.sleep(3000);
		
		String text = driver.findElement(By.linkText("Build your own computer")).getText();
		Assert.assertNotSame(text, "Build your own computer");
		
	}
	@Test(priority = 4)
	public void Logout() {
		driver.findElement(By.partialLinkText("Logout")).click();
	}
	@AfterClass
	public void closeBrowser() {
		driver.close();
	}
}
